==============================================================
Server Side
==============================================================

If your site uses server-side scripting you can use Server Side Arrow instead. It is more secure than Client Side.
To start implementing Arrow, follow the steps below:

1. Add Arrow Merchant Javascript to your website following the code below.

.. code-block:: html

    <!-- Arrow Merchant (Server Side) JS -->
    <script type="text/javascript" src="http://hi.projectarrow.co/cdn/arrow-secure.js"></script>

    <!-- jQuery Library . if already added dont add it again-->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
    crossorigin="anonymous"></script>

2. Get the Order Token from the server-side. Look at :doc:`generating_token`.

3. Launch Arrow Checkout by calling the launchArrow() function with the generated token.

.. code-block:: javascript

    launchArrow("INSERT_TOKEN_HERE");

