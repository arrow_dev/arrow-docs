=======================
Cancel API
=======================
If the customer wants to cancel or void an Order from Arrow you need to call the Cancel API with the order hash as seen below:

1. Do a POST Request to Cancel API

.. code-block:: bash

    $ curl https://fly.witharrow.co/api/order/cancel/<order_hash> \
    -u '<client_key>:<client_secret>' \
    -H 'Content-Type: application/json' \


.. list-table:: URL
   :widths: 25 75
   :header-rows: 1

   * - Variable (Type)
     - Description
   * - order_hash (String)
     - Order Hash 

.. list-table:: Authentication Header
   :widths: 25 75
   :header-rows: 1

   * - Variable (Type)
     - Description
   * - client_key (String)
     - Merchant Client Key
   * - client_secret (Integer)
     - Merchant Encryption or Secret Key

Response

.. code-block:: javascript

    {
        "success": 1,
        "message": "success",
        "data": {}
    }

.. list-table:: Error Code
   :widths: 50 50
   :header-rows: 1

   * - Error Message
     - Description
   * - Failed to do a cancel transaction
     - Arrow has an issue voiding the transaction
   * - no order found with this order_id on this merchant
     - Invalid Order Hash code
   * - Missing Order Hash
     - You need to pass the order_hash in order to cancel an order
   * - Authentication Header Missing
     - Make sure to pass Authentication Header with client_key and client_secret
   * - Invalid Merchant Client or Secret Key. Please Try again
     - You entered the wrong client_key and client_secret combination