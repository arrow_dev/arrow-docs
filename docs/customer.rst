=======================
Passing Customer Data
=======================

If you wish to have the user fill in their details on your website, or already store the user details for pre-existing customers, you can pass this data to Arrow so that it will be automatically filled when using Arrow Checkout. This makes it easier for returning customers and saves time.

1. Set the customer field with the following format 

.. code-block:: javascript

    "customer" : [
      {
        "first_name" : "User",
        "last_name" : "Name",
        "email" : "user@gmail.com",
        "contact" : "+12345678",
       
        "street" : "Example Street",
        "building" : "Example Building",
        "line2" : "Address Line 2",
        "city" : "Example City",
        "country" : "Singapore",
        "country_code" : "SG",
        "postal_code" : "12345",
    }
    ];

.. list-table:: Customer
   :widths: 25 75
   :header-rows: 1

   * - Variable (Type)
     - Description
   * - first_name (String)
     - First Name of the customer
   * - last_name (String)
     - Last Name of the customer
   * - email (String)
     - Customer Email
   * - contact (String)
     - Customer Phone Number (using + at the start)
   * - street (String)
     - Street Name
   * - building (String)
     - Building name and/or number
   * - line_2 (String)
     - Address Line 2 . leave it empty if not needed
   * - city (String)
     - City Name
   * - country (String)
     - Country Name
   * - country_code (String)
     - Country Code (for example Singapore is SG)
   * - postal_code (String)
     - Postal Code

