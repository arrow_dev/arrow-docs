===========================================
Encrypting data (Client Side)
===========================================

Arrow uses encryption to send the Arrow cart data to the Arrow server to make it more secure for the customer and the merchant.
To encrypt the data you need to complete the arrowOrder object with the data like items, shipping, etc.

1. Import arrow.js (an encryption mechanism is required)

.. code-block:: html

    <!-- Arrow Merchant JS -->
    <script type="text/javascript" src="http://shop.witharrow.co/cdn/arrow.js"></script>

2. Initialize arrow.js with your merchant information

.. code-block:: javascript

    initArrow({
        username: "my-shop-name",
        encryptionKey: "adfadfa3fhj0gf1g4ghg24g42g24" ,
        clientKey: "g420g2hg42g0h2ghrwgo42g2g"
    });

.. list-table:: initArrow
   :widths: 25 75
   :header-rows: 1

   * - Variable (Type)
     - Description
   * - username (String)
     - Arrow Merchant Username
   * - encryptionKey (String)
     - Arrow Merchant Encryption or Secret Key 
   * - clientKey (String)
     - Arrow Merchant Client or Merchant Key

3. Use launchArrow function to encrypt and send the data to Arrow Server and then launch a popup or redirect to an Arrow Checkout

.. code-block:: javascript

    launchArrow(arrowOrder); //arrowOrder is a variable name that store the arrow data

