===========================
Generating Token
===========================

These are the instructions for setting up Arrow Checkout on a server running with PHP. 

Requirement :
- PHP with openssl

1. Get the JSON data from client or the server, note that using the server is more secure.

2. Add the arrowEncrypt function to your PHP script as follows:

.. code-block:: php

    function arrowEncrypt($data, $key)
        {
            $method = 'AES-256-CBC';
            $length = openssl_cipher_iv_length($method);
            $iv = openssl_random_pseudo_bytes($length);
            $encrypted = openssl_encrypt($data, $method, $key, OPENSSL_RAW_DATA, $iv);
            if ($encrypted) {
                return base64_encode($encrypted) . '|' . base64_encode($iv);
            } else {
                return false;
            }
        }

3. Pass the JSON and merchant encryption key to the arrowEncrypt functon.

.. code-block:: php

    $data = arrowEncrypt($json,$merchant_encryption_key);

.. list-table:: initArrow
   :widths: 25 75
   :header-rows: 1

   * - Variable (Type)
     - Description
   * - $json (String)
     - JSON text data to encrypt (if it is an object you can json_encode it )
   * - $merchant_encryption_key (String)
     - Arrow Merchant Encryption Key 

4. Pass the data and merchant username to Arrow server using the arrowInit API. Look at :doc:`init` for more instructions on this step.