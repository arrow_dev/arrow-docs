=================================================================
Client Side
=================================================================

Arrow Implementation only use client javascript . meaning you can easily intergrate Arrow on static site without any other api or backend

if your site use server-side scripting you can use Server Side Arrow instead . it is more secure than Client Side . look at :doc:`implementation_secure`

To start implementing Arrow . follow the steps below:

1.Add Arrow Merchant Javascript to your website

.. code-block:: html

    <!-- Arrow Merchant JS -->
    <script type="text/javascript" src="http://hi.projectarrow.co/cdn/arrow.js"></script>


2.Initialize Arrow Merchant with your Arrow Merchant Username , Client/Merchant Key and Encryption Key . you can get the detail on your Arrow Merchant Site 

.. code-block:: javascript

   initArrow({
        username:"USERNAME",
        encryptionKey:'ENCRYPTION_KEY',
        clientKey:"CLIENT_KEY",
    });


.. list-table:: initArrow
   :widths: 25 75
   :header-rows: 1

   * - Variable (Type)
     - Description
   * - username (String)
     - Arrow Merchant Username
   * - encryptionKey (String)
     - Arrow Merchant Encryption Key 
   * - clientKey (String)
     - Arrow Merchant Client or Merchant Key

3.Start Adding Items to arrowOrder.items Array with the following content

.. code-block:: javascript

    arrowOrder.items.push(
        {
            name : "Arrow T-Shirt",
            quantity : 1,
            image: "https://cdn.arrow.co/image/arrow_tshirt.png",
            price : 19.99, 
            description : "Our Special Arrow T-Shirt",
            option : "White",
            extraData : {"variant_id":42069420} //variant_id is required for shopify
        }
    );

.. list-table:: Item
   :widths: 25 75
   :header-rows: 1

   * - Variable (Type)
     - Description
   * - name (String)
     - The name of the item
   * - quantity (int)
     - How much of the item is 
   * - image (String)
     - Image URL (absolute URL)
   * - price (float)
     - Price of the item
   * - description (String)
     - Item Description
   * - option (String)
     - Item Option . like Color or Size for example
   * - extraData (json) (Optional)
     - Extra Data to Pass

4.Optionally add a preapplied promocode and data to pass through

.. code-block:: javascript

    arrowOrder.promocode = ""; //preapplied promo code . optional
    arrowOrder.extraData = {id:69}; //you can pass the user_id or anything


.. list-table:: Optional Data
   :widths: 25 75
   :header-rows: 1

   * - Variable (Type)
     - Description
   * - promocode (String) optional
     - Preapplied promo code
   * - extraData (JSON Object) optional
     - Extra Data to pass 

5.Add a Shipping method . look at :doc:`shipping`

6.Optionally you can pass a customer data to Arrow . if the data isnt exist on arrow it will use the passed data . look at :doc:`customer`

7.Add the redirect when the order successful (required) . failed or canceled . look at :doc:`redirect`

8.Launch Arrow Checkout by using launchArrow() Function . optionally you can also clear the arrowOrder array . what this do is encrypt the json data and pass it to Arrow server . and Arrow will return a token that can be used to make An arrow checkout/purchase . for how init api work look at :doc:`init`

.. code-block:: javascript

    launchArrow();
    arrowOrder.items = [];


.. list-table:: Arrow Launch
   :widths: 25 75
   :header-rows: 1

   * - Function
     - Description
   * - launchArrow();
     - Launch the arrow checkout modal with data from arrowOrder object

