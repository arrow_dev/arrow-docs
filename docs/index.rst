.. arrowdocs documentation master file, created by
   sphinx-quickstart on Wed Feb 10 09:47:06 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Arrow Checkout documentation!
=========================================
Arrow is a Checkout Service. The Documentation contain everything you need to setup Arrow on your website

.. toctree::
   :maxdepth: 2
   :caption: Custom Implementation
      
   init
   implementation_secure
      
.. toctree::
   :maxdepth: 2
   :caption: Passing Information
      
   items
   shipping
   customer
   redirect
   other_data
   generating_token

.. toctree::
   :maxdepth: 2
   :caption: Arrow API
      
   orderdetail
   refund
   cancel

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
