=======================
Init API
=======================
In order to generate the Order Token and initialize a new order from Arrow you need to call the init API by following these steps:

1. Do a POST Request to Init API

.. code-block:: bash

    $ curl https://fly.witharrow.co/api/init 


.. list-table:: Data (POST)
   :widths: 25 75
   :header-rows: 1

   * - Variable (Type)
     - Description
   * - data (String)
     - Encrypted json data. Encoded with base64 
   * - username (String)
     - Merchant username (can be seen on seller portal) 

Response

.. code-block:: javascript

    {
        "success": 1,
        "message": "temporary order successfully created",
        "data": {
            "token" : "ABCDEF123456ABCDEF123456"
        }
    }

.. list-table:: Error Code
   :widths: 50 50
   :header-rows: 1

   * - Error Message
     - Description
   * - Invalid Base64
     - The passed data is invalid base64
   * - Invalid Username
     - The username is invalid or not found
   * - Tampered Data
     - Decryption of data failure. make sure to use correct encryption key while encrypting
   * - No order found with this order_id on this merchant
     - Invalid Order Hash code
   * - Invalid JSON
     - Invalid JSON Format. can be caused by decryption failure or invalid formatting
   * - Invalid Merchant
     - Invalid Client key on passed data
