===============
Items
===============

In order to use Arrow checkout you need to pass in the items from the consumer's cart. The items field will store the consumer's choices in a list/array.

1. Start Adding Items to items Array with the following content

.. code-block:: javascript

    "items" : [
        {
            name : "Arrow T-Shirt",
            quantity : 1,
            image: "https://cdn.arrow.co/image/arrow_tshirt.png",
            price : 19.99, 
            description : "Our Special Arrow T-Shirt",
            option : "White",
            extraData : {"variant_id":42069420} //variant_id is required for shopify
        }
    ]


.. list-table:: Item
   :widths: 25 75
   :header-rows: 1

   * - Variable (Type)
     - Description
   * - name (String)
     - The name of the item
   * - quantity (int)
     - How much of the item is 
   * - image (String)
     - Image URL (absolute URL)
   * - price (float)
     - Price of the item
   * - description (String)
     - A brief description of the item
   * - option (String)
     - Different choices like color or size
   * - extraData (json) (Optional)
     - Extra Data
