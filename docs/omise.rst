=======================
Omise
=======================
Omise Implementation

Requirement
############
In order to use Omise with arrow you will need to provide :

.. list-table:: Requirement
   :widths: 25 75
   :header-rows: 1

   * - Variable (Type)
     - Description
   * - api_key (String)
     - Omise API Key
   * - api_secret (String)
     - Omise API Secret
