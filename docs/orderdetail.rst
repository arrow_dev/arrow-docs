=======================
Order Detail API
=======================

This API can be used to fetch all the information related to the current order in a clear manner. 
You must call the API with order hash to get a response. A nested structure is returned as the response as seen in the response body below. 

If you are handeling the "success" redirect, you want to access the data that is stored in the Order structure within the response body of this API call. Again, 
this structure and its contents can be viewed in the code body below. 

Follow these steps to access the Order Detail API:

1. Do a POST Request to Order Detail API

.. code-block:: bash

    $ curl https://fly.witharrow.co/api/order/<order_hash> \
    -u '<client_key>:<client_secret>' \
    -H 'Content-Type: application/json' \


.. list-table:: URL
   :widths: 25 75
   :header-rows: 1

   * - Variable (Type)
     - Description
   * - order_hash (String)
     - Order Hash 

.. list-table:: Authentication Header
   :widths: 25 75
   :header-rows: 1

   * - Variable (Type)
     - Description
   * - client_key (String)
     - Merchant Client Key
   * - client_secret (Integer)
     - Merchant Encryption or Secret Key

Response

.. code-block:: javascript

    {
        "Order": {
            "order_hash": "DAC2E886",
            "time": null,
            "estimate": "Flat rate",
            "shipping": "Flat rate",
            "shipping_price": 5,
            "tax": 0,
            "subtotal": 86.6,
            "total": 91.6,
            "payment": "test visa spreedly",
            "title": "White T-Shirt x2 | Backpack x1 | Water Bottle x1 | ",
            "status": "confirmed",
            "payment_status": "authorized",
            "is_notified": 0,
            "extra_data": {
                "cart_id": "c2b207ee0c8f261ba9ad1ecdca2b2510",
                "order_id": 225
            },
            "items": [
                {
                    "id": 3208,
                    "time": null,
                    "name": "White T-Shirt",
                    "image": "http://wp.projectarrow.co/wp-content/uploads/2021/03/Kirkland-Signature-crew-neck-T-shirts-_6-pack-150x150.jpg",
                    "description": "This is our special white T-Shirt.",
                    "quantity": 2,
                    "price": 19.9,
                    "option": "\"\""
                },
                {
                    "id": 3209,
                    "time": null,
                    "name": "Backpack",
                    "image": "http://wp.projectarrow.co/wp-content/uploads/2021/03/backpack-150x150.jpg",
                    "description": "This is our special backpack.",
                    "quantity": 1,
                    "price": 39.9,
                    "option": "\"\""
                },
                {
                    "id": 3210,
                    "time": null,
                    "name": "Water Bottle",
                    "image": "http://wp.projectarrow.co/wp-content/uploads/2021/03/water-bottle-150x150.jpg",
                    "description": "This is our special water bottle.",
                    "quantity": 1,
                    "price": 6.9,
                    "option": "\"\""
                }
            ],
            "address": {
                "profile_name": "Igor Hoogerwoord",
                "firstname": "Igor",
                "lastname": "Hoogerwoord",
                "contact": "+6587502810",
                "building_type": null,
                "building_name": null,
                "lobby_name": null,
                "street": null,
                "floor": null,
                "unit": "13a",
                "address": "Trengganu St, Singapore 058467",
                "city": "Singapore",
                "state": "Singapore",
                "country": "Singapore",
                "postal_code": "058467",
                "latitude": null,
                "longitude": null
            },
            "user": {
                "id": 86,
                "first_name": "Igor",
                "last_name": "Hoogerwoord",
                "email": "igor@projectarrow.co"
            },
            "card": {
                "name": "test visa spreedly",
                "provider": "VISA",
                "ending": "1111",
                "exp": "11/2024"
            },
            "gateway": "stripe",
            "gateway_response": {
                "specific_response_fields": {
                    "stripe": {
                        "outcome_network_status": "approved_by_network",
                        "outcome_risk_level": "normal",
                        "outcome_seller_message": "Payment complete.",
                        "outcome_type": "authorized",
                        "stripe_source_brand": "Visa",
                        "stripe_source_country": "US",
                        "stripe_source_fingerprint": "56kKHe6vKC1NqSEF",
                        "stripe_source_funding": "credit",
                        "webhook_id": "we_1IaIKOIL9bjWO2ksfIhSrT2P"
                    }
                },
                "transaction_id": "ch_1IcigsIL9bjWO2ksQvBt04nd"
            },
            "shipping_list": [
                {
                    "title": "Flat rate",
                    "short_text": "Flat",
                    "order_display": "Flat rate",
                    "fee": 5,
                    "fee_display": null,
                    "selected": 1
                },
                {
                    "title": "Free shipping",
                    "short_text": "Free",
                    "order_display": "Free shipping",
                    "fee": 0,
                    "fee_display": null,
                    "selected": 0
                },
                {
                    "title": "Local pickup",
                    "short_text": "Loca",
                    "order_display": "Local pickup",
                    "fee": 5,
                    "fee_display": null,
                    "selected": 0
                }
            ]
        }
    }

.. list-table:: Error Code
   :widths: 50 50
   :header-rows: 1

   * - Error Message
     - Description
   * - Order Not Found
     - Invalid Order Hash
