==============================================
Tax , Promocodes, extraData, and Merchant client key
==============================================

There are a few additional fields that need to be updated for an order so that it is successful. 

The first is a tax field which is optional and can be used to add a fixed amount of tax. If your site has already calculated the tax for an order, this field can be used. The tax field will accept a float value representing the tax amount. 

.. code-block:: javascript

    "tax" : {
        "amount" : 10.00,
        "rate" : 0.1,
        "shipping_inclusive" : true
    }

.. list-table:: Tax
   :widths: 25 75
   :header-rows: 1

   * - Variable (Type)
     - Description
   * - amount (Decimal) (optional) (Default : 0.00)
     - Tax Amount
   * - rate (Decimal) (optional) (Default : 0.00)
     - Tax rate in decimal. 10% is 0.1 
   * - shipping_inclusive (Boolean) (optional) (Default : False)
     - if set to true shipping is also counted as tax 

Another field is a promocode field which is optional and will store any promocode that is applied by the customer to
the current order. Note that this is also an optional field.  

.. code-block:: javascript

    "promocode" : "PROMO"
    

Another important field in the order is the extraData field which is used to store any additional information about 
the order. Since it is a required field, if you have no extra data that needs to be added to the order you can simply 
store your website URL within the field as seen below. Note that this is a required field.

.. code-block:: javascript

    "extraData" : {
      "site": "url"
      }
    

Finally, you need to update a field for the merchant client key. The field will be updated with a string representing
the merchant client key as seen below: 

.. code-block:: javascript

    "merchant_client_key" : "d3c8667d5530e891a4a1b97bca0ea2e7"
    



.. list-table:: Other Data
   :widths: 25 75
   :header-rows: 1

   * - Variable (Type)
     - Description
   * - promocode (String) (Optional)
     - Preapplied promo code
   * - extraData (JSON Object) 
     - Extra Data to pass into the order, store the website URL if no extra data is needed.
   * - merchant_client_key (String)
     - Stores the merchant client key in the order. 
