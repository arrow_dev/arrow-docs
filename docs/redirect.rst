=========
Redirect
=========
You need to set the redirect field to store the URLs that the user should be redirected to depending on the 
outcome of their order. There are three possibilities: Success, Fail, and Cancel.

If the order is successfuly checked out, the users must be redirected to a page that displays that their order was successful. 
However, to verify the order properly you must fetch the order id as well as the Arrow order hash. Using this fetched data we must call the
Order Detail API and check if the order was a success or failure (See :doc:`orderdetail` for more details). Once we verify the order was a success,
we can update the data on the merchant website as well as Arrow to reflect the successful order.

If the order fails to be completed due to problems with the payment or data passed in by customer you need to redirect the user to a page that reflects
this information and gives them the opportunity to retry. 

If the order is cancelled by the user they should be returned to your home webpage or any webpage that confirms their order was cancelled. THe redirect field can be setup accordingly:

1. Set the redirect field with the following format 

.. code-block:: javascript

    "redirect" : {
        "success" : "https://example.com/success",
        "fail" : "https://example.com/fail",
        "cancel" : "https://example.com/cancel"
    }

.. list-table:: Redirect
   :widths: 25 75
   :header-rows: 1

   * - Variable (Type)
     - Description
   * - success (String)
     - URL to redirect to when the order is successful
   * - fail (String)
     - URL to redirect to when the order is failed
   * - cancel (String)
     - URL to redirect to when the order is canceled/declined


