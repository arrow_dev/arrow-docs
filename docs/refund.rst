=======================
Refund API
=======================
If a customer chooses to refund an order or a part of their order from Arrow, you need to call the Refund API. This can be done easily:

1. Do a POST Request to Refund API

.. code-block:: bash

    $ curl https://fly.witharrow.co/api/order/refund/<order_hash>/<amount> \
    -u '<client_key>:<client_secret>' \
    -H 'Content-Type: application/json' \


.. list-table:: URL
   :widths: 25 75
   :header-rows: 1

   * - Variable (Type)
     - Description
   * - order_hash (String)
     - Order Hash 
   * - Amount (Optional) (Integer)
     - Amount to refund in cents. $1 = 100 . If not set or null it will refund the whole amount 

.. list-table:: Authentication Header
   :widths: 25 75
   :header-rows: 1

   * - Variable (Type)
     - Description
   * - client_key (String)
     - Merchant Client Key
   * - client_secret (Integer)
     - Merchant Encryption or Secret Key

Response

.. code-block:: javascript

    {
        "success": 1,
        "message": "success",
        "data": {}
    }

.. list-table:: Error Code
   :widths: 50 50
   :header-rows: 1

   * - Error Message
     - Description
   * - Failed to do a refund transaction
     - Arrow has an issue refunding the transaction
   * - No Refund needed. already refunded all amount
     - The order have been fully refunded. no more refund is required or allowed
   * - Amount field need to be in cents
     - Make sure to enter only number in cents. for example $6.90 is 690
   * - no order found with this order_id on this merchant
     - Invalid Order Hash code
   * - Missing Order Hash
     - You need to pass the order_hash in order to refund an order
   * - Authentication Header Missing
     - Make sure to pass Authentication Header with client_key and client_secret
   * - Invalid Merchant Client or Secret Key. Please Try again
     - You entered the wrong client_key and client_secret combination