================
Shipping Method
================

Arrow get the available shipping methods from each merchant using the javascript data that they pass to Arrow Checkout

1. The shipping field is a list of structures used to store the details of the shipping methods that will be used for the order. Each structure within the list
will be populated as shown below: 

.. code-block:: javascript

  "shipping" : [
        {
            "title" : "Free",
            "description" : "Arrives 6-9 Sep",
            "fee" : 0.00,
            "fee_display" : "FREE"
            //if country is null it ship to all country
        },
        {
            "title" : "Economy",
            "short_text" : "Eco",
            "description" : "Arrives 4-20 Nov",
            "fee" : 50.00,
            "tax" : 0.07,
            "tax_shipping_incl" : true,
            "country" : "SG" //make this shipping only for singapore
        },
        {
            "title" : "Pro",
            "short_text" : "Pro",
            "description" : "Arrives 17 Des - 7 Jan",
            "fee" : 100.00,
            "tax" : 0.1,
            "tax_shipping_incl" : false,
            "country" : "ID"
        }

    ]


.. list-table:: Shipping
   :widths: 25 75
   :header-rows: 1

   * - Variable (Type)
     - Description
   * - title (String)
     - The name of the shipping method
   * - short_text (String) (Optional)
     - Short name for the shipping method, around 3-4 letters.
   * - description (String)
     - Description of the Shipping Method
   * - tax (Float)
     - How much percentage is the tax for the selected shipping (0.1 is 10% 0.05 is 5%) 
   * - tax_shipping_incl (Float)
     - If true shipping will also be taxed . if false only the subtotal of the items will be taxed
   * - fee (Float)
     - How much the shipping method cost
   * - fee_display (String) optional
     - The text to show when selecting the fee
 

