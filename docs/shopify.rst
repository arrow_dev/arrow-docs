==================
Shopify
==================
Shopify Implementation

Requirement
############
In order to use Shopify with arrow you will need to provide :

.. list-table:: Requirement
   :widths: 25 75
   :header-rows: 1

   * - Variable (Type)
     - Description
   * - api_key (String)
     - Shopify API Key 
   * - api_password (String)
     - Shopify API Password 
   * - store_name (String)
     - Shopify Store name (before .myshopify.com) 


Webhook 
########
Add Shopify Webhook to automatically mark order as refund or cancel . Currently webhook only support refund and cancel

Shopify Webhook URL :
::

    https://fly.witharrow.co/api/webhook/shopify/<client_key>

.. list-table:: URL
   :widths: 25 75
   :header-rows: 1

   * - Variable (Type)
     - Description
   * - client_key (String)
     - Merchant Client Key

