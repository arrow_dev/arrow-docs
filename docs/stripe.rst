=======================
Stripe
=======================
Stripe Implementation

Requirement
############
In order to use Stripe with arrow you will need to provide :

.. list-table:: Requirement
   :widths: 25 75
   :header-rows: 1

   * - Variable (Type)
     - Description
   * - login (String)
     - Stripe API Secret Key 